# js-tastyhash

Hash incoming Javascript (or other data) and compare against a list of hashes of "known-good" executable objects.

## User Story

You load a website in a brower. This website loads many Javascript files from many different domains per https://arxiv.org/pdf/1901.07699
After these files load but *before they are executed* js-tastyhash hashes each file. js-tastyhash then looks up each resulting hash in a table of trusted hashes.
Hashes which are present in the table are passed along for execution (to Noscript, right??) while all others are blocked.

## Other bits

How this table is constructed, by whom, and how the user trusts that party are all issues that need be resolved for this to work

### Evaluating js objects for "safety"
### How and who do you trust?
#### transitivity issues